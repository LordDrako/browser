#include <QtCore/QProcess>
#include <QtCore/QDebug>
#include <QtCore/QThread>

#include <QtGui/QWindow>

#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QTabWidget>

void openChild(QString const & title, QString const & url, QTabWidget * parent)
{
    QString program(qApp->applicationDirPath() + QStringLiteral("/child"));
    QStringList args;
    args << url;

    QProcess * childProc = new QProcess(parent);
    QObject::connect(qApp, &QApplication::aboutToQuit, childProc, &QProcess::close);
    QObject::connect(childProc, (void(QProcess::*)(QProcess::ProcessError))&QProcess::error, [](QProcess::ProcessError err){
        qDebug() << "FATAL ERROR:" << err;
    });
    QObject::connect(childProc, &QProcess::readyReadStandardOutput, [=](){
        auto winid = QString::fromUtf8(childProc->readLine()).toULongLong();

        qDebug() << winid;

        QWindow * window = QWindow::fromWinId(static_cast<WId>(winid));
        window->setObjectName(title);
        QWidget * embedded = QWidget::createWindowContainer(window, parent, Qt::ForeignWindow);

        parent->addTab(embedded, title);
        parent->setCurrentIndex(parent->count() - 1);

        embedded->update();
    });
    childProc->start(program, args, QIODevice::ReadOnly);

    QThread::msleep(200);
}

int main(int argc, char ** argv)
{
    QApplication a(argc, argv);

    QMainWindow mw;

    QTabWidget * tw = new QTabWidget(&mw);

    mw.setCentralWidget(tw);

    openChild(QStringLiteral("Google"), QStringLiteral("https://www.google.de"), tw);
    openChild(QStringLiteral("Yahoo!"), QStringLiteral("https://de.yahoo.com"), tw);
    openChild(QStringLiteral("bing"), QStringLiteral("https://www.bing.com"), tw);
    openChild(QStringLiteral("DuckDuckGo"), QStringLiteral("https://duckduckgo.com"), tw);

    mw.showMaximized();

    return a.exec();
}
