#include <iostream>

#include <QtWidgets/QApplication>

#include <QtWebEngineWidgets/QWebEngineView>

int main(int argc, char ** argv)
{
    QApplication a(argc, argv);

    QWebEngineView browser;
    browser.setWindowFlags(Qt::FramelessWindowHint);
    browser.setAttribute(Qt::WA_NativeWindow);
    browser.setUrl(QUrl(argc == 2 ? argv[1] : "https://www.google.de"));
    browser.show();

    std::cout << browser.winId() << std::endl;

    return a.exec();
}
